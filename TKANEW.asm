;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

.include "m8515def.inc"
.def temp = r16 ; temporary register
.def led_data = r17
.def counter = r21
.def wattTot = r22 
.def temp2 = r23 ; Temporary register that will store status of items that currently loaded
.def temp3 = r20;
.def counter_led = r24 ; for counter_led
.def A  = r25
.def status1 = r6 ;status untuk barang 1
.def status2 = r7 ;status untuk barang 2
.def status3 = r8 ;status untuk barang 3
.def status4 = r9 ;status untuk barang 4
;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

.org $00
rjmp MAIN
.org $01
rjmp ext_int0
.org $07
rjmp ISR_TOV0
.org $02
rjmp ext_int1
.org $0D
rjmp ext_int2

;====================================================================
; CODE SEGMENT
;====================================================================

.equ BLOCK1 = $60		;start address of SRAM array
.equ watt1 = 2 
.equ watt2 = 5
.equ watt3 = 1
.equ watt4 = 2

MAIN:

INIT_STACK:
	ldi temp, low(RAMEND)
	out SPL,temp
	ldi temp, high(RAMEND)
	out SPH, temp
	ldi	YH,high(BLOCK1)
	ldi	YL,low(BLOCK1)	;init Y-pointer
	ldi counter,0
	ldi temp2, 48
	ldi temp3,0
	ldi wattTot,0
	mov status1, temp2
	mov status2, temp2
	mov status3, temp2
	mov status4, temp2
	st Y+,counter
	st Y+,counter
	st Y+,counter
	st Y+,counter
	st Y+,counter
	mov r5,counter

INIT_LED:
	ser temp ; load $FF to temp
	out DDRC,temp ; Set PORTA to output
	rcall INIT_LCD_MAIN

INIT_INTERRUPT:
	ldi temp,0b00001010
	out MCUCR,temp
	ldi temp,0b11100000
	out GICR,temp
	ldi temp,0b00000000
	out EMCUCR,temp
	sei

LED_LOOP:
	cpi wattTot,9
	brge WARNING_FUNC
	rcall LED_CONF 
	out PORTC,led_data ; Update LFEDS
 	rjmp LED_LOOP

LED_CONF:
	cpi wattTot, 7
	brge LED_CONF_3
	cpi wattTot, 5
	brge LED_CONF_2
	cpi wattTot, 3
	brge LED_CONF_1
	ldi led_data, 0x00
	ret

LED_CONF_3:
	ldi led_data, 0x07
	ret

LED_CONF_2:
	ldi led_data, 0x03
	ret

LED_CONF_1:
	ldi led_data, 0x01
	ret

	

LED_LOOP_WARNING:
	tst r5
	brne MAIN
    out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	push led_data
	ldi led_data,0
	out PORTC,led_data
	rcall DELAY_01
	pop led_data
	rjmp LED_LOOP_WARNING

WARNING_FUNC: ;Warning Function for Exceeding Maximum Watt here, Timer Start Here
	ldi ZL,low(2*warning)
	ldi ZH,high(2*warning)
	rcall INIT_LCD
	rcall LOADBYTE
	rcall INIT_TIMER
	ldi led_data,0xFF
	rjmp LED_LOOP_WARNING

INIT_TIMER:
	ldi temp, (1<<CS12 | 1<<CS10); 
	out TCCR0,temp		
	ldi temp,1<<TOV0
	out TIFR,temp		; Interrupt if overflow occurs in T/C0
	ldi temp,1<<TOIE0
	out TIMSK,temp		; Enable Timer/Counter0 Overflow int
	sei
	ret


ISR_TOV0:
	inc counter_led
	cpi counter_led, 3
	breq RESET
	reti


RESET:
	ldi led_data, 0x00
	ldi counter_led,0
	ldi temp, (0<<CS12 | 0<<CS10); 
	out TCCR0,temp		
	ldi temp,0<<TOV0
	out TIFR,temp		; Interrupt if overflow occurs in T/C0
	ldi temp,0<<TOIE0
	out TIMSK,temp		; Enable Timer/Counter0 Overflow int
	ldi temp,1
	mov r5,temp
	sei
	reti

ext_int1: ;Next Interrupt
	cpi counter,4
	breq counter_2
	subi counter,-1
	rcall INIT_LCD_MAIN
	reti

counter_2:
	ldi counter,1
	rcall INIT_LCD_MAIN
	reti

ext_int2: ;Prev Interrupt
	cpi counter,1
	breq counter_1
	subi counter,1
	rcall INIT_LCD_MAIN
	reti

counter_1:
	ldi counter,4
	rcall INIT_LCD_MAIN
	reti

message_0:
	ldi ZH,high(2*begin)
	ldi ZL,low(2*begin)
	ret

message_1:
	ldi ZH,high(2*barang1)
	ldi ZL,low(2*barang1)
	ldi temp,watt1
	mov temp2,status1
	rjmp GET_STATUS

message_2:
	ldi ZH,high(2*barang2)
	ldi ZL,low(2*barang2)
	ldi temp,watt2
	mov temp2,status2
	rjmp GET_STATUS

message_3:
	ldi ZH,high(2*barang3)
	ldi ZL,low(2*barang3)
	ldi temp,watt3
	mov temp2,status3
	rjmp GET_STATUS

message_4:
	ldi ZH,high(2*barang4)
	ldi ZL,low(2*barang4)
	ldi temp,watt4
	mov temp2,status4
	rjmp GET_STATUS

GET_STATUS:
	add YL,counter
	ld temp3, Y 
	ldi YL,low(BLOCK1)
	ret

INPUT_TEXT:
	cpi counter,1
	breq message_1
	cpi counter,2
	breq message_2
	cpi counter,3
	breq message_3
	cpi counter,4
	breq message_4
	rjmp message_0

INIT_LCD_MAIN:
	rcall INIT_LCD

	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output

	rcall INPUT_TEXT
	rjmp LOADBYTE

LOADBYTE:
	lpm ; Load byte from program memory into r0

	tst r0 ; Check if we've reached the end of the message
	breq END_LCD ; If so, quit

	mov A, r0 ; Put the character onto Port B
	rcall WRITE_TEXT
	adiw ZL,1 ; Increase Z registers
	rjmp LOADBYTE

END_LCD:
	cpi wattTot,9
	brge END_LCD_2

	sbi PORTA,1
	out PORTB,temp2
	sbi PORTA,0
	cbi PORTA,0

	sbi PORTA,1
	ldi A, $20
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0
	
	cpi temp3,9
	brge END_LCD_BATAS
	sbi PORTA,1
	ldi A, 48
	add A, temp3
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0

	cbi PORTA,1
	ldi A, $C0
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0

	sbi PORTA,1
	ldi A, 48
	add A, wattTot
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0
	ret

END_LCD_BATAS:
	sbi PORTA,1
	ldi A, 57
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0

	cbi PORTA,1
	ldi A, $C0
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0

	sbi PORTA,1
	ldi A, 48
	add A, wattTot
	out PORTB,A
	sbi PORTA,0
	cbi PORTA,0
	ret

END_LCD_2:
	ret

INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi A,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	cbi PORTA,1 ; CLR RS
	ldi A,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi A,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi A,$01 ; MOV DATA,0x01
	out PORTB,A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ret

WRITE_TEXT:
	sbi PORTA,1 ; SETB RS
	out PORTB, A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ret

ext_int0:
	cpi temp2,49
	breq off
	add wattTot, temp
	add YL,counter
	ld temp, Y
	subi temp,-1 
	st Y,temp
	ldi YL,low(BLOCK1)
	ldi temp2,49
	rcall update_status
	rcall INIT_LCD_MAIN
	reti

off:
	ldi temp2,48
	sub wattTot, temp
	rcall update_status
	rcall INIT_LCD_MAIN
	reti

update_status:
	cpi counter,1
	breq status_1
	cpi counter,2
	breq status_2
	cpi counter,3
	breq status_3
	cpi counter,4
	breq status_4
	ret

status_1:
	mov status1,temp2
	ret

status_2:
	mov status2,temp2
	ret

status_3:
	mov status3,temp2
	ret

status_4:
	mov status4,temp2
	ret

DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz

	    ldi  r18, 52
	    ldi  r19, 242
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	ret

DELAY_00:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 4 000 cycles
	; 500us at 8.0 MHz

	    ldi  r18, 6
	    ldi  r19, 49
	L0: dec  r19
	    brne L0
	    dec  r18
	    brne L0
	ret
;====================================================================
; DATA
;====================================================================


begin:
	.db "0 watt Usage ",0

barang1:
	.db "Computer ",0 ;Wattnya 4

barang2:
	.db "Washer ",0 ;Wattnya 2

barang3:
	.db "TV ",0 ;Wattnya 3

barang4:
	.db "AC ",0 ;Wattnya 4

warning:
	.db "Watt maksimum",0

