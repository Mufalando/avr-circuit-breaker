Program ini berfungsi untuk memonitor jumlah watt listrik yang kita gunakan pada saat itu, 
dan memastikan memastikan supaya kita tidak memakai listrik lebih dari batas wajar. 
Setiap barang diasumsikan mempunyai watt yang stabil, dan LCD akan menampilkan jumlah watt 
total yang digunakan pada saat itu.

Akan ada 3 stage warning:
1)	Warning 1 ( LED 0 Menyala ) = ketika total watt <= 3
2)	Warning 2 ( LED 1 Menyala ) = ketika 3 < total watt <= 5
3)	Warning 3 ( LED 2 Menyala ) = ketika 5 < total watt <= 8
4)	Timer hingga semua LED berkedip jika total watt = 9
5)	Program Selesai

Terdapat 3 button yang tersedia meliputi :

1)	ON/OFF per alat elektronik
2)	Next : alat elektronik selanjutnya
3)	Prev : alat elektronik sebelumnya

User diharap bisa sadar kapan harus mematikan dan menghidupkan suatu alat 
elektronik, dengan warning pada LED dan LCD yang kita sediakan, sehingga dapat 
menghindari jatuh listrik.

Notes : Counter hanya bisa menghitung sampai 9